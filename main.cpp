#include <iostream>

void	PrintNumbers(int start, int end, bool IsEven)
{
	start = IsEven;
	for (start; start <= end; start = start + 2)
	{
			std::cout << start << " ";
	}
	std::cout << "\n";
}

int		main()
{
	const int	N = 20;
	PrintNumbers(0, N, false);
}
